from rest_framework import generics
from rest_framework import permissions
from .serializers import BucketListSerializer
from .models import BucketList
from .permissions import IsOwner


class CreateView(generics.ListCreateAPIView):
    """Defines create behavior of rest api."""

    queryset = BucketList.objects.all()
    serializer_class = BucketListSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)

    def perform_create(self, serializer):
        """Save post data when creating new bucketlist."""
        serializer.save(owner=self.request.user)


class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = BucketList.objects.all()
    serializer_class = BucketListSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
