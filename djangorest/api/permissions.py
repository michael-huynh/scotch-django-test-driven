from rest_framework.permissions import BasePermission
from .models import BucketList


class IsOwner(BasePermission):
    """
    Custom permission class to allow only the owner edit their bucketlists.
    """

    def has_object_permission(self, request, view, obj):
        if isinstance(obj, BucketList):
            return obj.owner == request.user

        return obj.owner == request.user

