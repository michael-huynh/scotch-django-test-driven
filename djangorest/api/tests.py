from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from .models import BucketList


class ModelTestCase(TestCase):
    """This class defines test suite for the bucketlist model."""

    def setUp(self):
        user = User.objects.create_user(username='nerd')
        self.bucketlist_name = 'Write world class code'
        self.bucketlist = BucketList(name=self.bucketlist_name, owner=user)

    def test_model_can_create_a_bucket_list(self):
        old_count = BucketList.objects.count()
        self.bucketlist.save()
        new_count = BucketList.objects.count()
        self.assertNotEqual(old_count, new_count)


class ViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define test client and other variables."""
        user = User.objects.create(username='nerd')

        self.client = APIClient()
        self.client.force_authenticate(user=user)

        self.bucketlist_data = {'name': 'Go to Code', 'owner': user.id}
        self.response = self.client.post(
            reverse('create'),
            self.bucketlist_data,
            format='json'
        )

    def test_api_can_create_bucket_list(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_a_bucket_list(self):
        """Test the api can get a given bucket list."""
        bucketlist = BucketList.objects.get()
        # response = self.client.get(
        #     reverse('details', kwargs={'pk': bucketlist.id}),
        #     format='json'
        # )
        response = self.client.get(
            reverse('details', kwargs={'pk': bucketlist.id}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertContains(response, bucketlist)

    def test_api_can_update_a_bucket_list(self):
        """Test the api can update a given bucket list."""
        bucketlist = BucketList.objects.get()
        change_bucketlist = {'name': 'New Name from Michael'}
        res = self.client.put(
            reverse('details', kwargs={'pk': bucketlist.id}),
            change_bucketlist,
            format='json'
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_a_bucketl_ist(self):
        """Test the api can delete a given bucket list."""
        bucketlist = BucketList.objects.get()
        res = self.client.delete(
            reverse('details', kwargs={'pk': bucketlist.id}),
            format='json',
            follow=True
        )
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
